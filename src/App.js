import React, {Component} from 'react';
import Game from './components/Game'
import './App.css'
import MyContext from './MyContext'

class App extends Component {
    changeValue = (id) => {
        const dupSquare = this.state.square.slice();
        if (dupSquare[id - 1] == "") {
            document.querySelectorAll('.gameSquare').forEach(function(square){
                square.classList.remove('game-square-focused')
            })
            document.getElementById('square-'+id).classList.add('game-square-focused')
            dupSquare[id - 1] = this.state.cross ? "O" : "X";
            this.setState({
                square: dupSquare,
                cross: !this.state.cross
            })
        }
        this.checkWinner(dupSquare, dupSquare[id - 1]);
    };

    checkWinner = (square, value) => {
        const  a = [
            [0, 1, 2],
            [0, 3, 6],
            [0, 4, 8],
            [1, 4, 7],
            [2, 5, 8],
            [2, 4, 6],
            [3, 4, 5],
            [6, 7, 8]
        ];
        a.map((b) => {
            const [e, f, g] = b;
            if (square[e] == value && square[f] == value && square[g] == value) {
                this.setState({
                    message: value + " is winner"
                });

                setTimeout(() => {
                    this.terminateGame();
                }, 1200)

            }
        })
    };

    terminateGame = () => {
        this.setState({
            square: Array(9).fill(''),
            message: "New game"
        })
    };

    state = {
        status: false,
        cross: false,
        message: 'Tic-Tac-Toe',
        square: Array(9).fill(''),
    };

    render() {
        return (
            <MyContext.Provider value={{
                value: this.state,
                changeValue: this.changeValue,
                resetGame: this.terminateGame
            }}>
                <Game/>
            </MyContext.Provider>
        );
    }
}

export default App;