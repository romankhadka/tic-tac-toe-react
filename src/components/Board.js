import React, {Component} from 'react';
import Square from "./Square";
import MyContext from "../MyContext";

class Board extends Component {
    render() {
        return (
            <MyContext.Consumer>
                {(context) => (
                    <React.Fragment>
                        <p className="mb-5 fa-2x">{context.value.message}</p>
                        <table>
                            <tbody>
                            <tr>
                                <td><Square id={"1"}/></td>
                                <td><Square id={"2"}/></td>
                                <td><Square id={"3"}/></td>
                            </tr>
                            <tr>
                                <td><Square id={"4"}/></td>
                                <td><Square id={"5"}/></td>
                                <td><Square id={"6"}/></td>
                            </tr>
                            <tr>
                                <td><Square id={"7"}/></td>
                                <td><Square id={"8"}/></td>
                                <td><Square id={"9"}/></td>
                            </tr>
                            </tbody>
                        </table>
                        <button onClick={context.resetGame} className="mt-5 btn btn-danger">New Game</button>
                    </React.Fragment>
                )}

            </MyContext.Consumer>

        );
    }
}

export default Board;