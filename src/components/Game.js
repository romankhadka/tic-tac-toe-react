import React, {Component} from 'react';
import Board from "./Board";
import MyContext from "../MyContext";

class Game extends Component {
    render() {
        return (
            <MyContext.Consumer>
                {(context)=>(
                    <div className="mt-4 text-center d-flex justify-content-between">
                        <div className="w-25 align-items-center d-flex justify-content-center">
                            <i className="fa fa-times" style={{color: context.value.cross? "": "green", fontSize: "200px"}}></i>
                        </div>
                        <div className="mt-5">
                            <Board/>
                        </div>
                        <div className="w-25 align-items-center d-flex justify-content-center">
                            <i className="fa fa-circle-thin fa-5x" style={{color: context.value.cross? "green": "", fontSize: "170px"}}></i>

                        </div>
                    </div>
                )}
            </MyContext.Consumer>

        );
    }
}

export default Game;