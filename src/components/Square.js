import React, {Component} from 'react';
import MyContext from '../MyContext'

class Square extends Component {
    markup = (context) => (
        <div className="gameSquare" id={"square-"+this.props.id}>
            <button className="btn game-square"
                    onClick={context.changeValue.bind(this, this.props.id)}>
                {context.value.square[this.props.id - 1]}
            </button>
        </div>
    )

    render() {
        return (
            <MyContext.Consumer>
                {this.markup}
            </MyContext.Consumer>

        );
    }
}

export default Square;